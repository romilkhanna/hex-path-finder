require 'Benchmark'
require_relative 'HexBoard'

def run(locations, obstacles, verbose=false)
	@board = HexBoard.new(locations, obstacles, verbose)

	@board.stats[:bfs][:time] = @board.stats[:dfs][:time] = @board.stats[:ids][:time] = 0
	@board.stats[:greedy][:time] = @board.stats[:a_star][:time] = 0

	@board.stats[:bfs][:time] = (Benchmark.realtime { 1.times { @board.create_neighbours; @board.bfs(@board.get_random, @board.get_random) } } * 1000).round(2)
	@board.stats[:dfs][:time] = (Benchmark.realtime { 1.times { @board.create_neighbours; @board.dfs(@board.get_random, @board.get_random) } } * 1000).round(2)
	@board.stats[:ids][:time] = (Benchmark.realtime { 1.times { @board.create_neighbours; @board.ids(@board.get_random, @board.get_random) } } * 1000).round(2)
	@board.stats[:greedy][:time] = (Benchmark.realtime { 1.times { @board.create_neighbours; @board.greedy(@board.get_random, @board.get_random) } } * 1000).round(2)
	@board.stats[:a_star][:time] = (Benchmark.realtime { 1.times { @board.create_neighbours; @board.a_star(@board.get_random, @board.get_random) } } * 1000).round(2)

	print_stats("bfs")
	print_stats("dfs")
	print_stats("ids")
	print_stats("greedy")
	print_stats("a_star")
end

def print_stats(algorithm)
	percent_visited = (@board.stats[algorithm.to_sym][:visited].to_f/@board.stats[algorithm.to_sym][:possible_connections]*100).round(2)
	total_run_time = @board.stats[algorithm.to_sym][:time]
	nodes_per_ms = (@board.stats[algorithm.to_sym][:visited] / (@board.stats[algorithm.to_sym][:time])).ceil

	line = "Stats for: #{@board.stats[algorithm.to_sym][:name]}\n"
	line << "Number of nodes created: #{@board.stats[algorithm.to_sym][:node_instances]}\n"
	line << "Number of nodes visited: #{@board.stats[algorithm.to_sym][:visited]} "
	line << "out of a possible #{@board.stats[algorithm.to_sym][:possible_connections]}: "
	line << "#{percent_visited}%\n"
	line << "Number of problems solved: #{@board.stats[algorithm.to_sym][:solved]}\n"
	line << "Nodes per ms: #{nodes_per_ms}\n"
	line << "Total run time: #{total_run_time} milliseconds.\n\n"
	puts line
end

run(20, 0.5, true)
