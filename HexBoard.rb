include Math
require_relative 'Hex'

class HexBoard
	attr_reader :stats

	def initialize(locations, obstacles, verbose=false)
		@obstacles = obstacles
		@stats = {
			:bfs => { :solved => 0, :visited => 0, :node_instances => 0, :possible_connections => 0, :name => "bfs" },
			:dfs => { :solved => 0, :visited => 0, :node_instances => 0, :possible_connections => 0, :name => "dfs" },
			:ids => { :solved => 0, :visited => 0, :node_instances => 0, :possible_connections => 0, :name => "ids" },
			:greedy => { :solved => 0, :visited => 0, :node_instances => 0, :possible_connections => 0, :name => "greedy" },
			:a_star => { :solved => 0, :visited => 0, :node_instances => 0, :possible_connections => 0, :name => "a_star" },
		}
		@verbose = verbose

		# make a grid just exactly or bigger than what we need
		sq = sqrt(locations).ceil
		@rows, @cols = sq, sq

		# add requested number of nodes on the grid
		added = 0
		@nodes ||= {}
		@rows.times { |r|
			next if added == locations
			@nodes[r] ||= {}
			@cols.times { |c|
				added == locations ? next : added += 1
				@nodes[r][c] = Hex.new({ icon: 'o', pos: [r,c], neighbours: [], paths: {} })
			}
		}
	end

	def draw
		# draw square grid as hexes
		@nodes.each { |row, col|
			line = ""
			(@cols - row).times { line << ' ' }

			col.values.each { |node|
				line << "#{node.opts[:icon]}"
				line << (node.opts[:paths].has_key?(get_node([node.opts[:pos][0], node.opts[:pos][1]+1])) ? '-' : ' ')
			}
			puts line

			line = ""
			(@cols - row).times { line << ' ' }
			col.values.each { |node|
				bottom_left = node.opts[:paths].has_key?(get_node([node.opts[:pos][0]+1, node.opts[:pos][1]]))
				bottom_right = node.opts[:paths].has_key?(get_node([node.opts[:pos][0]+1, node.opts[:pos][1]+1]))
				if bottom_left && bottom_right
					line << '/\\'
				elsif bottom_left
					line << '/'
				elsif bottom_right
					line << '\\'
				else
					line << ' '						
				end
			}
			puts line
		}
		puts ""
	end

	def absolute_distance(start, goal)
		# Euclidean distance
		return sqrt((start.opts[:pos][0]-goal.opts[:pos][0])**2 + (start.opts[:pos][1]-goal.opts[:pos][1])**2).ceil
	end

	def get_node(pos)
		return nil if @nodes[pos[0]].nil? || @nodes[pos[0]][pos[1]].nil? # empty row or column
		return @nodes[pos[0]][pos[1]]
	end

	def get_random
		until (node = get_node([rand(@rows), rand(@cols)])); end
		return node
	end

	def print_paths
		# print all the weights that exist
		puts "Paths:"
		@nodes.each { |row, col|
			col.values.each { |node|
				line = "Pos: #{node.opts[:pos]}, Paths: "
				node.opts[:paths].each { |neighbour, weight|
					line << "#{neighbour.opts[:pos]}=>#{weight} "
				}
				puts line
			}
		}
		puts ""
	end

	def print_result(path, success=false)
		draw
		line = (success ? "Path found, visited: " : "No path found, visited: ")
		path.each { |n|
			line << "#{n.opts[:pos]} "
		}
		puts line
		puts ""
	end

	def print_banner(start, goal)
		puts "Starting at: #{start.opts[:pos]}, going to: #{goal.opts[:pos]}\n\n" if @verbose
		start.opts[:icon] = 's'
		goal.opts[:icon] = 'g'
	end

	def create_neighbours
		@connections = 0
		@nodes.each { |row, col|
			col.values.each { |node|
				r, c = node.opts[:pos]
				neighbours = [
					[r, c-1], [r-1, c-1], # left, top left
					[r-1, c], [r, c+1], # top (becomes top right), right
					[r+1, c+1], [r+1, c], # bottom right, bottom (becomes bottom left)
				].delete_if { |x,y| x < 0 || y < 0 || x > @rows || y > @cols || @nodes[x].nil? || @nodes[x][y].nil? }
				
				neighbours.each { |n|
					neighbour = get_node(n)

					# set random weight to neighbour
					node.opts[:paths][neighbour] = neighbour.opts[:paths][node] = rand(1..10)
				}
				node.opts[:icon] = 'o' # have to reset the icon
				@connections += node.opts[:paths].size
			}
		}
		@connections = (@connections / 2).floor

		# figure out how many connections have to be removed
		@remove = (@connections * @obstacles).ceil

		puts "Total connections: #{@connections}, #{@remove} connections will be removed.\n\n" if @verbose

		# randomly block some paths
		removed = 0
		until removed == @remove
			tile = get_random
			neighbour = tile.opts[:paths].keys.sample # random neighbour

			next if neighbour.nil? # in case the tile picked was an island

			# delete paths
			tile.opts[:paths].delete(neighbour)
			neighbour.opts[:paths].delete(tile)

			removed += 1
		end

	end

	def bfs(start, goal)
		q = [start]
		visited = []
		distance = 0

		@stats[:bfs][:possible_connections] += (@connections - @remove)
		print_banner(start, goal)

		solved = false
		while q.any?
			node = q.shift

			visited << node
			if node == goal
				solved = true
				break
			end

			node.opts[:icon] = (distance += 1).to_s unless node == start

			q.push(node.opts[:paths].keys - visited).flatten!
			q.uniq!
		end
		print_result(visited, solved) if @verbose
		@stats[:bfs][:solved] += 1 if solved
		@stats[:bfs][:visited] += visited.size
		@stats[:bfs][:node_instances] = Hex.num_instances
	end

	def dfs(start, goal)
		q = [start]
		visited = []
		distance = -1

		@stats[:dfs][:possible_connections] += (@connections - @remove)
		print_banner(start, goal)

		solved = false
		while q.any?
			node = q.shift

			visited << node
			if node == goal
				solved = true
				break
			end

			node.opts[:icon] = (distance += 1).to_s unless node == start

			q.unshift(node.opts[:paths].keys - visited).flatten!
			q.uniq!
		end
		print_result(visited, solved) if @verbose
		@stats[:dfs][:solved] += 1 if solved
		@stats[:dfs][:visited] += visited.size
		@stats[:dfs][:node_instances] = Hex.num_instances
	end

	def ids(start, goal)
		q = [start]
		future = []
		visited = []
		distance = -1

		@stats[:ids][:possible_connections] += (@connections - @remove)
		print_banner(start, goal)

		solved = false
		while q.any?
			node = q.shift

			visited << node
			if node == goal
				solved = true
				break
			end

			node.opts[:icon] = (distance += 1).to_s unless node == start

			future << node.opts[:paths].keys

			if q.empty?
				q = (future.flatten - visited).uniq
				future = []
			end
		end
		print_result(visited, solved) if @verbose
		@stats[:ids][:solved] += 1 if solved
		@stats[:ids][:visited] += visited.size
		@stats[:ids][:node_instances] = Hex.num_instances
	end	

	def greedy(start, goal)
		q = [start]
		visited = []
		distance = 0

		@stats[:greedy][:possible_connections] += (@connections - @remove)
		print_banner(start, goal)

		solved = false
		while q.any?
			node = q.shift

			visited << node
			if node == goal
				solved = true
				break
			end

			node.opts[:icon] = (distance += 1).to_s unless node == start

			node.opts[:paths].each { |neighbour, cost|
				neighbour.opts[:distance] = absolute_distance(neighbour, goal)
			}

			q.unshift(node.opts[:paths].keys.sort_by { |neighbour| neighbour.opts[:distance] } - visited).flatten!
			q.uniq!
		end
		print_result(visited, solved) if @verbose
		@stats[:greedy][:solved] += 1 if solved
		@stats[:greedy][:visited] += visited.size
		@stats[:greedy][:node_instances] = Hex.num_instances
	end

	def a_star(start, goal)
		q = [start]
		visited = []
		distance = 0

		@stats[:a_star][:possible_connections] += (@connections - @remove)
		print_banner(start, goal)

		solved = false
		while q.any?
			node = q.shift

			visited << node
			if node == goal
				solved = true
				break
			end

			node.opts[:icon] = (distance += 1).to_s unless node == start

			node.opts[:paths].each { |neighbour, cost|
				neighbour.opts[:distance] = absolute_distance(neighbour, goal)
				neighbour.opts[:distance] = 0 if neighbour.opts[:paths].keys.include?(goal)
				neighbour.opts[:distance] += cost
			}

			q.unshift(node.opts[:paths].keys.sort_by { |neighbour| neighbour.opts[:distance] } - visited).flatten!
			q.uniq!
		end
		print_result(visited, solved) if @verbose
		@stats[:a_star][:solved] += 1 if solved
		@stats[:a_star][:visited] += visited.size
		@stats[:a_star][:node_instances] = Hex.num_instances
	end
end