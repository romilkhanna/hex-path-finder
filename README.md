# README #

In order to get setup, simply checkout the repo and run the application as:

```
#!ruby

ruby main.rb
```

## Features and setup ##

HexPath is a path finding application written in Ruby which uses hex tile maps. The following algorithms are implemented in the application:

* Breadth-First Search (BFS)
* Depth-First Search (DFS)
* Iterative Deepening Search (IDS)
* Greedy Search
* A* Search

The application accepts two parameters which can be defined in main.rb prior to run:

* Number of tiles
* Number of obstacles as a decimal percentage

There is also a definition for enabling verbose mode which will show path taken and a graph representation. Some examples:

```
#!ruby

run(10, 0.5) # 10 locations with 50% obstacles, no verbose
```
```
#!ruby

run(10, 0.5, true) # 10 locations with 50% obstacles, verbose
```

### Who do I talk to? ###

I can be reached at romilkhanna@outlook.com for any questions or feature requests.