class Hex
	attr_accessor :opts
	@@instances = 0
	def initialize(opts={})
		@opts = opts
		@@instances += 1
	end

	public

	def self.num_instances
		@@instances
	end
end